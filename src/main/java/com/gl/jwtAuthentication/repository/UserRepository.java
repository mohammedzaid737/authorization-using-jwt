package com.gl.jwtAuthentication.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gl.jwtAuthentication.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, String> {

}
