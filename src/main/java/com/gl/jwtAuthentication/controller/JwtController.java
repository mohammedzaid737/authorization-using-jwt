package com.gl.jwtAuthentication.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gl.jwtAuthentication.model.JwtRequest;
import com.gl.jwtAuthentication.model.JwtResponse;
import com.gl.jwtAuthentication.service.JwtService;

@RestController
@CrossOrigin
@RequestMapping("/api")

public class JwtController {
	
	@Autowired
	private JwtService jwtService;
	
	@PostMapping({"/auth/signin"})
	public JwtResponse createJwtToken(@RequestBody JwtRequest jwtRequest)throws Exception
	{
		return jwtService.createJwtToken(jwtRequest);
	}
	
	

}
