package com.gl.jwtAuthentication.controller;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gl.jwtAuthentication.configuration.JwtRequestFilter;
import com.gl.jwtAuthentication.model.User;
import com.gl.jwtAuthentication.service.UserService;
import com.gl.jwtAuthentication.util.JwtUtil;

@RestController
@RequestMapping("/api")
public class UserController {

	@Autowired
	private UserService userServ;

	@Autowired
	private JwtUtil jwtUtil;

	@Autowired
	JwtRequestFilter jwtRequestFilter;
	
	String userName = null;

	@PostConstruct
	public void initRolesAndUserServ() {
		userServ.initRolesAndUser();
	}

	@PostMapping({"/auth/signup"})
	public User registerNewUser(@RequestBody User user) {
		User userSaved = userServ.registerNewUser(user);
		return userSaved;
	}

	@GetMapping({"/test/admin"})
	@PreAuthorize("hasRole('Admin')")
	public String forAdmin()
	{
		return "This URL is only accessible for Admin";
	}

	@GetMapping({"/test/user"})
	@PreAuthorize("hasRole('User')")
	public String forUser()
	{

		
		try
		{
			String jwtToken = jwtRequestFilter.getToken();
			userName = jwtUtil.getUserNameFromToken(jwtToken);
			String res = "Welcome "+ userName + "\n \n \nThis URL is only accessible for Registered User ";
			return res;
		}
		catch (Exception e) {
			System.out.println("some exce"+ e);
			return "This URL is only accessible for Registered User "+ userName;
		}
	}
	
	@GetMapping("/test/mod")
	@PreAuthorize("hasAnyRole('Admin','User')")
	public ResponseEntity<?> getModeratorContent()
	{
		String jwtToken = jwtRequestFilter.getToken();
		userName = jwtUtil.getUserNameFromToken(jwtToken);
		String res = "Welcome "+userName+"\n\n\nThis URL is accessible for both Admin and User" ;
		
		return new ResponseEntity<String>(res,HttpStatus.OK);
	}
	
	@GetMapping("/test/all")
	public ResponseEntity<?> getAllPublicContent()
	{
		String publicContent = "This Link has Public Access and is accessible to all the Users";
		return new ResponseEntity<String>(publicContent,HttpStatus.OK);
	}

}







