package com.gl.jwtAuthentication.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.gl.jwtAuthentication.model.Role;
import com.gl.jwtAuthentication.service.RoleService;

@RestController
public class RoleController {

	@Autowired
	RoleService serv;
	
	@PostMapping({"/createNewRole"})
	public Role createNewRole(@RequestBody Role role)
	{
		Role savedrole = serv.createNewRole(role);
		return savedrole;
	}
}
