package com.gl.jwtAuthentication.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gl.jwtAuthentication.model.Role;
import com.gl.jwtAuthentication.repository.RoleRepository;

@Service
public class RoleService {

	@Autowired
	RoleRepository roleRepo;
	
	public Role createNewRole(Role role)
	{
		Role savedRole = roleRepo.save(role);
		return savedRole;
	}
}
