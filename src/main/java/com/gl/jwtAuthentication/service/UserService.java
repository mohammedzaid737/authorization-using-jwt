package com.gl.jwtAuthentication.service;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.gl.jwtAuthentication.model.Role;
import com.gl.jwtAuthentication.model.User;
import com.gl.jwtAuthentication.repository.RoleRepository;
import com.gl.jwtAuthentication.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepo;
	
	@Autowired
	private RoleRepository roleRepo;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	
	public void initRolesAndUser() {
		
		Role adminrole = new Role();
		adminrole.setRoleName("Admin");
		adminrole.setRoleDescription("Admin role");
		roleRepo.save(adminrole);
		
		Role userrole = new Role();
		userrole.setRoleName("User");
		userrole.setRoleDescription("Default role for newly created User");
		roleRepo.save(userrole);
		
		User adminUser = new User();
		adminUser.setUserName("admin123");
		adminUser.setUserFirstName("admin");
		adminUser.setUserLastName("admin");
		adminUser.setUserPassword(getEncodedPassword("admin@pass"));
		Set<Role> adminRoles = new HashSet<>();
		adminRoles.add(adminrole);
		adminUser.setRole(adminRoles);
		userRepo.save(adminUser);
		
//		User user = new User();
//		user.setUserName("raj123");
//		user.setUserFirstName("raj");
//		user.setUserLastName("sharma");
//		user.setUserPassword(getEncodedPassword("raj@pass"));
//		Set<Role> userRoles = new HashSet<>();
//		userRoles.add(userrole);
//		user.setRole(userRoles);
//		userRepo.save(user);
		
	}
	public User registerNewUser(User user)
	{
		Role role = roleRepo.findById("User").get();
		Set<Role> userRoles = new HashSet<>();
		
		userRoles.add(role);
		user.setRole(userRoles);
		user.setUserPassword(getEncodedPassword(user.getUserPassword()));
		
		User userSaved = userRepo.save(user);
		
		return userSaved;
	}
	
	public String getEncodedPassword(String password)
	{
		return passwordEncoder.encode(password);
	}
}































