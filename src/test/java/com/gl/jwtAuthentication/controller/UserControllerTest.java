package com.gl.jwtAuthentication.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gl.jwtAuthentication.model.User;
import com.gl.jwtAuthentication.service.UserService;

class UserControllerTest {

	@Autowired
	MockMvc mockmvc;
	
	@InjectMocks
	UserController usercontroller;
	
	@MockBean
	UserService userservice;
	
	User user;
	
	@SuppressWarnings("deprecation")
	@BeforeEach
	public void setup() {
		
		MockitoAnnotations.initMocks(this);
        mockmvc=MockMvcBuilders.standaloneSetup(usercontroller).build();
        
        user=new User();
        user.setUserFirstName("mohammed");
        user.setUserLastName("zaid");
        user.setUserName("zaid123");
        user.setUserPassword("zaid@123");
        
	
	}
	
	@Test
	public void signupandreturnobject() throws Exception {
		
		Mockito.when(userservice.registerNewUser(user)).thenReturn(user);
		
		mockmvc.perform(MockMvcRequestBuilders.post("/user/api/SignUp")
				                               .contentType(MediaType.APPLICATION_JSON)
				                               .content(converobjtojson(user)))
		.andExpect(MockMvcResultMatchers.status().isOk())
												.andDo(MockMvcResultHandlers.print());
		
		
		
		
	}


	public String converobjtojson(Object obj) throws JsonProcessingException
	{
		ObjectMapper objmap=new ObjectMapper();
		String output=objmap.writeValueAsString(obj);
		return output;
		
		
	}

}
